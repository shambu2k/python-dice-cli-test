import random


class Dice:
    def __init__(self, lowerLimit, upperLimit):
        self.lowerLimit = lowerLimit
        self.upperLimit = upperLimit

    def getRandom(self):
        return 0 + random.randint(self.lowerLimit, self.upperLimit)
