import pathlib
from setuptools import setup, find_packages

HERE = pathlib.Path(__file__).parent

README = (HERE / "README.md").read_text()

# This call to setup() does all the work
setup(
    name="sample-dice",
    version="1.0.3",
    description="Generate random numbers",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/shambu2k/python-dice-cli-test",
    author="Sidharth Shambu",
    author_email="dummy@dummy.com",
    license="MIT",
    packages=find_packages(exclude=("tests",)),
)
