from sample_dice import Dice


def testDice():
    dice = Dice(1, 6)
    randomNum = dice.getRandom()
    assert(randomNum >= 1 and randomNum <= 6)


if __name__ == "__main__":
    testDice()
